using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelUnitPlacementEvent
{
    private static CancelUnitPlacementEvent instance;

    public static CancelUnitPlacementEvent GetInstance()
    {
        if (instance == null)
            instance = new CancelUnitPlacementEvent();
        return instance;
    }

    public delegate void CancelUnitPlacementDelegate(GameObject callerOfPlacement);
    public event CancelUnitPlacementDelegate CancelUnitPlacement;

    public void CancelPlacement(GameObject callerOfPlacement) => CancelUnitPlacement?.Invoke(callerOfPlacement);
}
