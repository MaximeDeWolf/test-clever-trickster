using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPlacementManager : MonoBehaviour
{
    private GameObject currentPlacementCaller;
    private GameObject unitToPlace;

    // Events
    private UnitSelectedForPlacementEvent unitSelectedForPlacementEvent;
    private CancelUnitPlacementEvent cancelUnitPlacementEvent;
    private UnitPlacementPerformedEvent unitPlacementPerformedEvent;
    private UnitCreationRequestEvent unitCreationRequestEvent;

    private void Awake()
    {
        unitSelectedForPlacementEvent = UnitSelectedForPlacementEvent.GetInstance();
        cancelUnitPlacementEvent = CancelUnitPlacementEvent.GetInstance();
        unitPlacementPerformedEvent = UnitPlacementPerformedEvent.GetInstance();
        unitCreationRequestEvent = UnitCreationRequestEvent.GetInstance();
    }

    private void OnEnable()
    {
        unitSelectedForPlacementEvent.UnitSelectedForPlacement += BeginUnitPlacement;
    }

    private void OnDisable()
    {
        unitSelectedForPlacementEvent.UnitSelectedForPlacement -= BeginUnitPlacement;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            PlaceUnitOnGrid();
    }

    /// <summary>
    /// Begin the display of the unit to place as a ghost.
    /// Also notifies the caller of the previous placement if it was cancel by this one
    /// </summary>
    /// <param name="caller">The object that triggered this event</param>
    /// <param name="unitSelected">The unit to place on the grid</param>
    private void BeginUnitPlacement(GameObject caller, GameObject unitSelected)
    {
        CancelCurrentPlacement();

        currentPlacementCaller = caller;
        unitToPlace = unitSelected;
    }

    /// <summary>
    /// Cancel the current unit placement if there is one.
    /// Notify the caller of the canceled placement.
    /// </summary>
    private void CancelCurrentPlacement()
    {
        if (currentPlacementCaller == null)
            return;

        cancelUnitPlacementEvent.CancelPlacement(currentPlacementCaller);
        currentPlacementCaller = null;
        unitToPlace = null;
    }

    private void PlaceUnitOnGrid()
    {
        GameObject gridSquareObject = GridSquareViewFinder.GetPointedGridSquare();

        if (gridSquareObject == null)
            return;

        IGridSquare gridSquare = gridSquareObject.GetComponent<IGridSquare>();
        if (gridSquare.IsOccupied() || gridSquare.SquareType != GridSquareType.Player)
            return;

        unitCreationRequestEvent.SendUnitCreationRequest(unitToPlace, gridSquareObject);
        unitPlacementPerformedEvent.NotifyUnitPlacementPerformed(currentPlacementCaller);

        currentPlacementCaller = null;
        unitToPlace = null;
    }
}
