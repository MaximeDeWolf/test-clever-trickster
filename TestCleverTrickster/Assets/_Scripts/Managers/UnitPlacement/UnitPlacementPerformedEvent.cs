using UnityEngine;

/// <summary>
/// The event triggered when a unit placement has been performed successfully 
/// </summary>
public class UnitPlacementPerformedEvent
{
    private static UnitPlacementPerformedEvent instance;

    public static UnitPlacementPerformedEvent GetInstance()
    {
        if (instance == null)
            instance = new UnitPlacementPerformedEvent();
        return instance;
    }

    public delegate void UnitPlacementPerformedDelegate(GameObject callerOfPlacement);
    public event UnitPlacementPerformedDelegate UnitPlacementPerformed;

    public void NotifyUnitPlacementPerformed(GameObject callerOfPlacement) => UnitPlacementPerformed?.Invoke(callerOfPlacement);
}
