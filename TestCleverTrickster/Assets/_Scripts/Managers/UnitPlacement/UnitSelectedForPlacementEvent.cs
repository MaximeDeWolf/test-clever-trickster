using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitSelectedForPlacementEvent
{
    private static UnitSelectedForPlacementEvent instance;

    public static UnitSelectedForPlacementEvent GetInstance()
    {
        if (instance == null)
            instance = new UnitSelectedForPlacementEvent();
        return instance;
    }

    public delegate void UnitSelectedForPlacementDelegate(GameObject caller, GameObject unitSelected);
    public event UnitSelectedForPlacementDelegate UnitSelectedForPlacement;

    public void SelectUnitForPlacement(GameObject caller, GameObject unitSelected) => UnitSelectedForPlacement?.Invoke(caller, unitSelected);
}
