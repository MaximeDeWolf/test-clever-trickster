using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPlacementRenderer : MonoBehaviour, IUnitPlacementRenderer
{
    [Header("The materials to apply to the unit to place")]
    [Tooltip("The default ghost material to apply to the unit to place")]
    [SerializeField]
    private Material defaultGhostMaterial;
    [Tooltip("The ghost material to apply to the unit when tryining to place it in a valid square")]
    [SerializeField]
    private Material greenGhostMaterial;
    [Tooltip("The ghost material to apply to the unit when tryining to place it in a invalid square")]
    [SerializeField]
    private Material redGhostMaterial;

    private GameObject unitToRender;
    private Camera cam;

    // Events
    private UnitSelectedForPlacementEvent unitSelectedForPlacementEvent;
    private CancelUnitPlacementEvent cancelUnitPlacementEvent;
    private UnitPlacementPerformedEvent unitPlacementPerformedEvent;

    private void Awake()
    {
        unitSelectedForPlacementEvent = UnitSelectedForPlacementEvent.GetInstance();
        cancelUnitPlacementEvent = CancelUnitPlacementEvent.GetInstance();
        unitPlacementPerformedEvent = UnitPlacementPerformedEvent.GetInstance();
    }

    void Start()
    {
        cam = Camera.main;
    }

    private void OnEnable()
    {
        unitSelectedForPlacementEvent.UnitSelectedForPlacement += ListenUnitSelectedForPlacement;
        cancelUnitPlacementEvent.CancelUnitPlacement += StopUnitRender;
        unitPlacementPerformedEvent.UnitPlacementPerformed += StopUnitRender;
    }
    private void OnDisable()
    {
        unitSelectedForPlacementEvent.UnitSelectedForPlacement -= ListenUnitSelectedForPlacement;
        cancelUnitPlacementEvent.CancelUnitPlacement -= StopUnitRender;
        unitPlacementPerformedEvent.UnitPlacementPerformed -= StopUnitRender;
    }

    // Update is called once per frame
    void Update()
    {
        RenderUnit();
    }

    /// <summary>
    /// Render the unit at the screen, either at the mouse pisition or at the center of a grid square if possible
    /// </summary>
    private void RenderUnit()
    {
        if (unitToRender == null)
            return;

        GameObject gridSquare = GridSquareViewFinder.GetPointedGridSquare();
        if (gridSquare == null)
            RenderUnitOnMousePosition();
        else
            RenderUnitOnSnappedPosition(gridSquare);
    }

    /// <summary>
    /// Render the unit to place at the center of the grid square pointed by the mouse
    /// </summary>
    /// <param name="gridSquare">The grid square game object pointed</param>
    private void RenderUnitOnSnappedPosition(GameObject gridSquare)
    {
        IGridSquare gridSquareScript = gridSquare.GetComponent<IGridSquare>();
        IMaterialsHandler materialsHandler = unitToRender.GetComponent<IMaterialsHandler>();

        unitToRender.transform.position = gridSquareScript.GetCenter();
        materialsHandler.RemoveExtraMaterials();

        switch (gridSquareScript.SquareType)
        {
            case GridSquareType.Neutral:
                materialsHandler.AddMaterial(defaultGhostMaterial);
                break;
            case GridSquareType.Player:
                if(gridSquareScript.IsOccupied())
                    materialsHandler.AddMaterial(redGhostMaterial);
                else
                    materialsHandler.AddMaterial(greenGhostMaterial);
                break;
            case GridSquareType.Enemy:
                materialsHandler.AddMaterial(redGhostMaterial);
                break;
        }

    }

    /// <summary>
    /// Render the unit to place at the world position pointed by the mouse
    /// </summary>
    private void RenderUnitOnMousePosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = cam.transform.position.y;
        Vector3 worldPosition = cam.ScreenToWorldPoint(mousePosition);
        worldPosition.y = 0;

        unitToRender.transform.position = worldPosition;

        IMaterialsHandler materialsHandler = unitToRender.GetComponent<IMaterialsHandler>();
        materialsHandler.RemoveExtraMaterials();
        materialsHandler.AddMaterial(defaultGhostMaterial);
    }

    /// <summary>
    /// Listen to the UnitSelectedForPlacementEvent in order to display the unit to place properly
    /// </summary>
    /// <param name="caller">Not used</param>
    /// <param name="unitToPlace">The unit to place on the grid</param>
    private void ListenUnitSelectedForPlacement(GameObject caller, GameObject unitToPlace)
    {
        SetUnitToRender(unitToPlace);
    }

    /// <summary>
    /// Listen to the CancelUnitPlacementEvent in order to stop to display thr unit to place
    /// </summary>
    /// <param name="caller"></param>
    private void StopUnitRender(GameObject caller)
    {
        UnsetUnitToRender();
    }

    // IUnitPlacementRenderer interfaces

    public void SetUnitToRender(GameObject unitToRender)
    {
        IMaterialsHandler materialsHandler = unitToRender.GetComponent<IMaterialsHandler>();
        if (unitToRender.GetComponent<IMaterialsHandler>() == null)
            return;

        this.unitToRender = Instantiate<GameObject>(unitToRender, transform);
        this.unitToRender.GetComponent<IMaterialsHandler>().AddMaterial(defaultGhostMaterial);
    }

    public void UnsetUnitToRender()
    {
        Destroy(unitToRender);
        unitToRender = null;
    }

    // --------------------------------
}
