using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnitPlacementRenderer
{
    /// <summary>
    /// Set the unit to render while the unit placement occurs
    /// </summary>
    /// <param name="unitToRender">The unit to render</param>
    void SetUnitToRender(GameObject unitToRender);

    /// <summary>
    /// Unset the unit to render so that this object does not render anything
    /// </summary>
    void UnsetUnitToRender();
}
