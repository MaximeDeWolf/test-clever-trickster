using UnityEngine;

/// <summary>
/// The event to request to create a unit in a grid square
/// </summary>
public class UnitCreationRequestEvent
{
    private static UnitCreationRequestEvent instance;

    public static UnitCreationRequestEvent GetInstance()
    {
        if (instance == null)
            instance = new UnitCreationRequestEvent();
        return instance;
    }

    public delegate void UnitCreationRequestDelegate(GameObject unitToCreate, GameObject gridSquare);
    public event UnitCreationRequestDelegate UnitCreationRequest;

    public void SendUnitCreationRequest(GameObject unitToCreate, GameObject gridSquare) => UnitCreationRequest?.Invoke(unitToCreate, gridSquare);
}
