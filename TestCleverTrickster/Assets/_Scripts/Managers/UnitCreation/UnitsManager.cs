using UnityEngine;

/// <summary>
/// Create the units at the right grid square
/// </summary>
public class UnitsManager : MonoBehaviour
{
    // Events
    private UnitCreationRequestEvent unitCreationRequestEvent;

    private void Awake()
    {
        unitCreationRequestEvent = UnitCreationRequestEvent.GetInstance();
    }

    private void OnEnable()
    {
        unitCreationRequestEvent.UnitCreationRequest += CreateUnit;
    }

    private void OnDisable()
    {
        unitCreationRequestEvent.UnitCreationRequest -= CreateUnit;
    }

    /// <summary>
    /// Create a unit and link it to the "squareGrid". Modify the world position of the new unit accordingly
    /// </summary>
    /// <param name="unitToCreate">The prefab of the unit to create</param>
    /// <param name="gridSquare">The grid square where we want to put the unit</param>
    private void CreateUnit(GameObject unitToCreate, GameObject gridSquareObject)
    {
        IGridSquare gridSquare = gridSquareObject.GetComponent<IGridSquare>();
        if (gridSquare == null)
            return;

        GameObject newUnit = Instantiate<GameObject>(unitToCreate, transform);
        gridSquare.OccupyingUnit = newUnit;
        newUnit.transform.position = gridSquare.GetCenter();
    }
}
