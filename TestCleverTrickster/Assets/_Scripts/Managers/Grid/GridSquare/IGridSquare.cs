using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GridSquareType
{
    Neutral,
    Player,
    Enemy
}

public interface IGridSquare
{
    GameObject OccupyingUnit { get; set; }
    GridSquareType SquareType { get; set; }

    /// <summary>
    /// Get the length of the side of this square (in Unity unit)
    /// </summary>
    /// <returns>The length of the side of this square </returns>
    float GetSquareSize();

    /// <summary>
    /// Set the new size of the square
    /// </summary>
    /// <param name="newSize">The new size of the square (in Unity units)</param>
    void SetSquareSize(float newSize);

    /// <summary>
    /// Sets the index position if this square in the grid and modify its relative position to the grid accordingly
    /// </summary>
    /// <param name="indexX">The index along the X axis</param>
    /// <param name="indexZ">The index along the Z axis</param>
    void SetIndexes(int indexX, int indexZ);

    /// <summary>
    /// Check if there is a unit in this square.
    /// </summary>
    /// <returns>Return true iif there is a unit in this square</returns>
    bool IsOccupied();

    /// <summary>
    /// Return the center of the grid square (in world position, not local)
    /// </summary>
    /// <returns>The center of the grid square</returns>
    Vector3 GetCenter();
}
