using UnityEngine;

/// <summary>
/// Enter Summary here
/// </summary>
public interface ISquareRenderer
{
    /// <summary>
    /// Render a square at the position of the object
    /// </summary>
    /// <param name="squareSize">The size of the square to render (in Unity units)</param>
    void RenderSquare(float squareSize);
}
