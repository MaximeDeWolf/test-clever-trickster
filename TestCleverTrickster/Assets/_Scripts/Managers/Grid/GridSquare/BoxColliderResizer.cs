using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(SquareSizeChangedEvent))]

/// <summary>
/// Resize the BoxCollider of this object when its size changes
/// </summary>
public class BoxColliderResizer : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<SquareSizeChangedEvent>().SquareSizeChanged += ResizeBoxCollider;
    }

    private void OnDisable()
    {
        GetComponent<SquareSizeChangedEvent>().SquareSizeChanged -= ResizeBoxCollider;
    }

    /// <summary>
    /// Resize the BoxCollider so that its new X and Z sizes match the 'newSize'
    /// </summary>
    /// <param name="newSize">The new size to apply to the BoxCollider (in Unity units)</param>
    private void ResizeBoxCollider(float newSize)
    {
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        boxCollider.size = new Vector3(newSize, boxCollider.size.y, newSize);
        boxCollider.center = new Vector3(newSize * 0.5f, boxCollider.center.y, newSize * 0.5f);
    }
}
