using UnityEngine;

/// <summary>
/// Enter Summary here
/// </summary>
public class SquareSizeChangedEvent : MonoBehaviour
{
    public delegate void SquareSizeChangedDelegate(float newSize);
    public event SquareSizeChangedDelegate SquareSizeChanged;

    public void ChangeSquareSize(float newSize) => SquareSizeChanged?.Invoke(newSize);
}
