using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(SquareSizeChangedEvent))]
public class SquareOutline : MonoBehaviour, ISquareRenderer
{

    private void OnEnable()
    {
        GetComponent<SquareSizeChangedEvent>().SquareSizeChanged += RenderSquare;
    }

    private void OnDisable()
    {
        GetComponent<SquareSizeChangedEvent>().SquareSizeChanged -= RenderSquare;
    }

    public void RenderSquare(float squareSize)
    {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        int numberOfVertices = 4;

        Vector3[] positions = new Vector3[numberOfVertices];
        positions[0] = new Vector3(0f, 0f, 0f);
        positions[1] = new Vector3(squareSize, 0f, 0f);
        positions[2] = new Vector3(squareSize, 0f, squareSize);
        positions[3] = new Vector3(0f, 0f, squareSize);

        lineRenderer.positionCount = numberOfVertices;
        lineRenderer.SetPositions(positions);
    }
}
