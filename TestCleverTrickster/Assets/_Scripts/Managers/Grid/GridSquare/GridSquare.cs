using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SquareSizeChangedEvent))]
public class GridSquare : MonoBehaviour, IGridSquare
{
    [Tooltip("The size of a side of this square grid (in Unity unit)")]
    [SerializeField]
    private float sideSize = 1;

    private int indexX = 0;
    private int indexZ = 0;

    // IGridSquare interfaces

    public GameObject OccupyingUnit { get; set; }
    public GridSquareType SquareType { get ; set ; }

    public float GetSquareSize()
    {
        return sideSize;
    }

    public void SetSquareSize(float newSize)
    {
        sideSize = newSize;
        GetComponent<SquareSizeChangedEvent>().ChangeSquareSize(newSize);
        transform.localPosition = new Vector3(indexX * sideSize, transform.position.y, indexZ * sideSize);
    }

    public bool IsOccupied()
    {
        return OccupyingUnit != null;
    }

    public void SetIndexes(int indexX, int indexZ)
    {
        this.indexX = indexX;
        this.indexZ = indexZ;
        transform.localPosition = new Vector3(indexX * sideSize, transform.position.y, indexZ * sideSize);
    }

    public Vector3 GetCenter()
    {
        return transform.position + new Vector3(GetSquareSize() * 0.5f, 0, GetSquareSize() * 0.5f);
    }

    // ------------------------------

    private void Awake()
    {
        SquareType = GridSquareType.Neutral;
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SquareSizeChangedEvent>().ChangeSquareSize(sideSize);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
