using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour, IGrid
{
    [Header("Size")]
    [Tooltip("The number of grid square to put along the X axis")]
    [SerializeField]
    private int sizeX = 5;
    [Tooltip("The number of grid square to put along the Z axis")]
    [SerializeField]
    private int sizeZ = 5;
    [Tooltip("The length of a grid square (in Unity units)")]
    [SerializeField]
    private float squareSize = 1f;

    [Header("")]
    [Tooltip("The prefab of a grid square")]
    [SerializeField]
    private GameObject gridSquarePrefab;

    [Header("Game zones")]
    [Tooltip("The game object that represents the player zone (where he can place its units)")]
    [SerializeField]
    private GameObject playerZone;
    [Tooltip("The game object that represents the enemy zone (where the IA can place its units)")]
    [SerializeField]
    private GameObject enemyZone;

    // Start is called before the first frame update
    void Start()
    {
        InstantiateGridSquare();
        SetGridSquaresType(enemyZone, GridSquareType.Enemy);
        SetGridSquaresType(playerZone, GridSquareType.Player);
        LinkGameZoneObjectsToGrid(enemyZone);
        LinkGameZoneObjectsToGrid(playerZone);
    }

    /// <summary>
    /// Create all the squares object of the grid
    /// </summary>
    private void InstantiateGridSquare()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                GameObject newGridItem = Instantiate<GameObject>(gridSquarePrefab, transform);
                IGridSquare gridSquare = newGridItem.GetComponent<IGridSquare>();
                if (gridSquare != null)
                {
                    gridSquare.SetIndexes(x, z);
                    gridSquare.SetSquareSize(squareSize);
                }
            }
        }
    }

    /// <summary>
    /// Set the square type of all the grid squares that are located inside the 'gameZone' to 'squareTypes'.
    /// We use the center of the grid square and the bounds of the 'gameZone' BoxCollider to do it.
    /// </summary>
    /// <param name="gameZone">An object that represents a game zone (must contain a BoxCollider component)</param>
    /// <param name="squareType">The type to assign to each grid square that are in the 'gameZone'</param>
    private void SetGridSquaresType(GameObject gameZone, GridSquareType squareType)
    {
        BoxCollider zoneBounds = gameZone.GetComponent<BoxCollider>();
        if (zoneBounds == null)
        {
            Debug.LogError("This game object must contain a BoxCollider component: " + gameZone.name);
            return;
        }

        Vector3 zoneBottomRight = gameZone.transform.position;
        Vector3 zoneUpperLeft = zoneBottomRight + new Vector3(-zoneBounds.size.x, 0, zoneBounds.size.z);

        foreach(Transform childSquare in transform)
        {
            IGridSquare gridSquare = childSquare.gameObject.GetComponent<IGridSquare>();
            Vector3 squareCenter = gridSquare.GetCenter();

            if (IsPositionInRectangle(squareCenter, zoneUpperLeft, zoneBottomRight))
                gridSquare.SquareType = squareType;
        }
    }

    /// <summary>
    /// Link existing game objects in the game zone to the grid. This will mark the matching grid square as occupied
    /// </summary>
    /// <param name="gameZone">An object that represents a game zone</param>
    private void LinkGameZoneObjectsToGrid(GameObject gameZone)
    {
        foreach (Transform child in gameZone.transform)
        {
            foreach (Transform childSquare in transform)
            {
                IGridSquare gridSquare = childSquare.gameObject.GetComponent<IGridSquare>();
                Vector3 squareBottomRight = childSquare.position;
                squareBottomRight.x += gridSquare.GetSquareSize();
                Vector3 squareUpperLeft = squareBottomRight + new Vector3(-gridSquare.GetSquareSize(), 0, gridSquare.GetSquareSize());

                if (IsPositionInRectangle(child.position, squareUpperLeft, squareBottomRight))
                    gridSquare.OccupyingUnit = child.gameObject;
            }
        }
    }

    /// <summary>
    /// Return true iif the point "position" is contained in the rectangle formed by the "upperLeftCorner" and the "bottomRightCorner".
    /// (the Y axis data is not take into account)
    /// </summary>
    /// <param name="position">The position to check</param>
    /// <param name="upperLeftCorner">The upper left corner of the rectangle</param>
    /// <param name="bottomRightCorner">The bottom right corner of the rectangle</param>
    /// <returns></returns>
    private bool IsPositionInRectangle(Vector3 position, Vector3 upperLeftCorner, Vector3 bottomRightCorner)
    {
        bool isXInZoneBounds = position.x > upperLeftCorner.x && position.x <= bottomRightCorner.x;
        bool isYInZoneBounds = position.z < upperLeftCorner.z && position.z >= bottomRightCorner.z;

        return isXInZoneBounds && isYInZoneBounds;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
