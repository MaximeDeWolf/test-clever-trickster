using UnityEngine;

/// <summary>
/// This is the interface of the game states
/// </summary>
public interface IGameState
{
    /// <summary>
    /// This method sets up the game state
    /// </summary>
    void EnterGameState(IGameStateManager gameStateManager);
}
