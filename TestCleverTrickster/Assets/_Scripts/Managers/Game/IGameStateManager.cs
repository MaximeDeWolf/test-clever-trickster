using UnityEngine;

/// <summary>
/// This is the interface for the game state manager
/// </summary>
public interface IGameStateManager
{
    PlacementGameState PlacementState { get;  }
    FightGameState FightState { get; }
    ResultsGameState ResultsState { get; }

    /// <summary>
    /// Returns the panel that allows the player to place its units
    /// </summary>
    /// <returns>The game object that represents the panel that allows the player to place its units</returns>
    GameObject GetUnitPlacementPanel();

    /// <summary>
    /// Returns the game object that represents the UnitPlacementManager
    /// </summary>
    /// <returns>The game object that represents the UnitPlacementManager</returns>
    GameObject GetUnitPlacementManager();

    /// <summary>
    /// Change the current game state to the new one
    /// </summary>
    /// <param name="newGameState">The new game state</param>
    void ChangeGameState(IGameState newGameState);
}
