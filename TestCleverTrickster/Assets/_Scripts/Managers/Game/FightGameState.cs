using UnityEngine;

/// <summary>
/// The game state that handles the fight phase of the game
/// </summary>
public class FightGameState : IGameState
{
    private IGameStateManager manager;

    // Events
    private FightStateBeginEvent fightStateBeginEvent;

    public FightGameState()
    {
        fightStateBeginEvent = FightStateBeginEvent.GetInstance();
    }

    public void EnterGameState(IGameStateManager gameStateManager)
    {
        manager = gameStateManager;

        fightStateBeginEvent.NotifyFightStateBegin();
    }
}
