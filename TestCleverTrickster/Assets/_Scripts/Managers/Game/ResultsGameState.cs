using UnityEngine;

/// <summary>
/// The game state that handles the results phase of the game
/// </summary>
public class ResultsGameState : IGameState
{
    private IGameStateManager manager;

    public void EnterGameState(IGameStateManager gameStateManager)
    {
        manager = gameStateManager;
    }
}
