using UnityEngine;


/// <summary>
/// This class handles the different states of the game
/// </summary>
public class GameStateManager : MonoBehaviour, IGameStateManager
{
    [Header("Context for the Placement state phase")]
    [Tooltip("The game object that represents the unit placement panel")]
    [SerializeField]
    private GameObject unitPlacementPanel;
    [Tooltip("The game object that represents the UnitPlacementManager")]
    [SerializeField]
    private GameObject unitPlacementManager;

    public IGameState CurrentGameState { get ; private set; }
    public PlacementGameState PlacementState { get; private set; }
    public FightGameState FightState { get; private set; }
    public ResultsGameState ResultsState { get; private set; }

    private void Awake()
    {
        PlacementState = new PlacementGameState();
        FightState = new FightGameState();
        ChangeGameState(PlacementState);
    }

    // IGameStateManager interface

    public void ChangeGameState(IGameState newGameState)
    {
        CurrentGameState = newGameState;
        newGameState.EnterGameState(this);
    }

    public GameObject GetUnitPlacementPanel()
    {
        return unitPlacementPanel;
    }

    public GameObject GetUnitPlacementManager()
    {
        return unitPlacementManager;
    }

    // -------------------------
}
