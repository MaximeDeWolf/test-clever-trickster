using UnityEngine;

/// <summary>
/// The game state that handles the units placement phase of the game
/// </summary>
public class PlacementGameState : IGameState
{
    private IGameStateManager manager;
    private GameObject unitPlacementManager;
    private GameObject unitPlacementPanel;

    // Events
    private AllUnitsPlacedEvent allUnitsPlacedEvent;

    public PlacementGameState()
    {
        allUnitsPlacedEvent = AllUnitsPlacedEvent.GetInstance();
    }

    /// <summary>
    /// Subscribe to the events needed by this state
    /// </summary>
    private void Subscribe()
    {
        allUnitsPlacedEvent.AllUnitsPlaced += ExitGameState;
    }

    /// <summary>
    /// Unsubscribe from all events
    /// </summary>
    private void Unsubscribe()
    {
        allUnitsPlacedEvent.AllUnitsPlaced -= ExitGameState;
    }

    /// <summary>
    /// Exit this game state and trigger the FightGameState
    /// </summary>
    private void ExitGameState()
    {
        Unsubscribe();
        unitPlacementManager.SetActive(false);
        unitPlacementPanel.SetActive(false);

        manager.ChangeGameState(manager.FightState);
    }

    // IGameState interface

    public void EnterGameState(IGameStateManager gameStateManager)
    {
        Subscribe();

        manager = gameStateManager;
        unitPlacementManager = manager.GetUnitPlacementManager();
        unitPlacementPanel = manager.GetUnitPlacementPanel();

        unitPlacementManager.SetActive(true);
        unitPlacementPanel.SetActive(true);
    }

    // ----------------------
}
