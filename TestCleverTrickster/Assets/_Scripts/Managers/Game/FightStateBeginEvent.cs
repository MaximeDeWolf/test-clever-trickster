using UnityEngine;

/// <summary>
/// This event is triggered when the Fight phase begins
/// </summary>
public class FightStateBeginEvent
{
    private static FightStateBeginEvent instance;

    public static FightStateBeginEvent GetInstance()
    {
        if (instance == null)
            instance = new FightStateBeginEvent();
        return instance;
    }

    public delegate void FightStateBeginDelegate();
    public event FightStateBeginDelegate FightStateBegin;

    public void NotifyFightStateBegin() => FightStateBegin?.Invoke();
}
