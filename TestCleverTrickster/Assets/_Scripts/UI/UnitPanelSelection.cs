using UnityEngine;

/// <summary>
/// The responsability of this class is to trigger an event when all the units have been placed on the grid
/// </summary>
public class UnitPanelSelection : MonoBehaviour
{

    private int remainingUnitToPlace;

    // Events
    private UnitPlacementPerformedEvent unitPlacementPerformedEvent;
    private AllUnitsPlacedEvent allUnitsPlacedEvent;

    private void Awake()
    {
        remainingUnitToPlace = 0;
        foreach (Transform child in transform)
            remainingUnitToPlace++;

        unitPlacementPerformedEvent = UnitPlacementPerformedEvent.GetInstance();
        allUnitsPlacedEvent = AllUnitsPlacedEvent.GetInstance();
    }

    private void OnEnable()
    {
        unitPlacementPerformedEvent.UnitPlacementPerformed += UpdateRemainingUnitsToPLace;
    }

    private void OnDisable()
    {
        unitPlacementPerformedEvent.UnitPlacementPerformed -= UpdateRemainingUnitsToPLace;
    }

    private void UpdateRemainingUnitsToPLace(GameObject caller)
    {
        remainingUnitToPlace --;
        if (remainingUnitToPlace == 0)
            allUnitsPlacedEvent.NotifyAllUnitsPlaced();
    }
}
