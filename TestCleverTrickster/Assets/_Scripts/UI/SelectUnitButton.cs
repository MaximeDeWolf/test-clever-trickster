using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
public class SelectUnitButton : MonoBehaviour
{
    [Tooltip("The prefabs of the unit to place on the grid")]
    [SerializeField]
    private GameObject unitToSpawn;

    private Color normalColor;

    // Events
    private UnitSelectedForPlacementEvent unitSelectedForPlacementEvent;
    private CancelUnitPlacementEvent cancelUnitPlacementEvent;
    private UnitPlacementPerformedEvent unitPlacementPerformedEvent;

    private void Awake()
    {
        normalColor = GetComponent<Button>().colors.normalColor;
        unitSelectedForPlacementEvent = UnitSelectedForPlacementEvent.GetInstance();
        cancelUnitPlacementEvent = CancelUnitPlacementEvent.GetInstance();
        unitPlacementPerformedEvent = UnitPlacementPerformedEvent.GetInstance();
    }

    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(SendUnitSelectedEvent);
        cancelUnitPlacementEvent.CancelUnitPlacement += HandleCanceledPlacement;
        unitPlacementPerformedEvent.UnitPlacementPerformed += HandlePlacementPerformed;
    }

    private void OnDisable()
    {
        GetComponent<Button>().onClick.AddListener(SendUnitSelectedEvent);
        cancelUnitPlacementEvent.CancelUnitPlacement -= HandleCanceledPlacement;
        unitPlacementPerformedEvent.UnitPlacementPerformed -= HandlePlacementPerformed;
    }

    /// <summary>
    /// Send an event to notify that a unit has been selected for placement.
    /// Also paint the button in 'Highligthed' color.
    /// </summary>
    private void SendUnitSelectedEvent()
    {
        // Change button color
        GetComponent<Image>().color = GetComponent<Button>().colors.highlightedColor;

        unitSelectedForPlacementEvent.SelectUnitForPlacement(gameObject, unitToSpawn);
    }

    /// <summary>
    /// Handle the cancellation of the current placement if this object was the caller
    /// </summary>
    /// <param name="caller">The object that triggered the 'unitSelectedForPlacementEvent' event</param>
    private void HandleCanceledPlacement(GameObject caller)
    {
        if (caller != gameObject)
            return;

        GetComponent<Image>().color = normalColor;
    }

    /// <summary>
    /// Set this button to not interactable anymore if this object was the caller of the performed unit placement
    /// </summary>
    /// <param name="caller">The object that triggered the 'unitSelectedForPlacementEvent' event</param>
    private void HandlePlacementPerformed(GameObject caller)
    {
        if (caller != gameObject)
            return;

        GetComponent<Button>().interactable = false;
    }
}
