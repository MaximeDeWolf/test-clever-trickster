using UnityEngine;

[RequireComponent(typeof(Animator))]

/// <summary>
/// This class allows to trigger a popup animation on a text
/// </summary>
public class TextPopup : MonoBehaviour
{
    // Events
    private FightStateBeginEvent fightStateBeginEvent;

    private void Awake()
    {
        fightStateBeginEvent = FightStateBeginEvent.GetInstance();
    }

    private void OnEnable()
    {
        fightStateBeginEvent.FightStateBegin += TriggerPopupAnimation;
    }

    private void OnDisable()
    {
        fightStateBeginEvent.FightStateBegin -= TriggerPopupAnimation;
    }

    private void TriggerPopupAnimation()
    {
        GetComponent<Animator>().SetTrigger("Popup");
    }
}
