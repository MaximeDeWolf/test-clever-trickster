using UnityEngine;

/// <summary>
/// This event is triggered when the player has placed all its units
/// </summary>
public class AllUnitsPlacedEvent
{
    private static AllUnitsPlacedEvent instance;

    public static AllUnitsPlacedEvent GetInstance()
    {
        if (instance == null)
            instance = new AllUnitsPlacedEvent();
        return instance;
    }

    public delegate void AllUnitsPlacedDelegate();
    public event AllUnitsPlacedDelegate AllUnitsPlaced;

    public void NotifyAllUnitsPlaced() => AllUnitsPlaced?.Invoke();
}
