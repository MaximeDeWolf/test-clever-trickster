using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMaterialsHandler
{
    /// <summary>
    /// Add a material to the materials list of the mesh renderer of this object
    /// </summary>
    /// <param name="material">The extra material to apply t the mesh renderer</param>
    void AddMaterial(Material material);

    /// <summary>
    /// Remove all materials of the messh renderer except the first one
    /// </summary>
    void RemoveExtraMaterials();
}
