using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMaterialsHandler : MonoBehaviour, IMaterialsHandler
{
    [Tooltip("The game object that contains the mesh renderer of the unit")]
    [SerializeField]
    private GameObject meshRenderObject;

    // IMaterialsHandler interfaces

    public void AddMaterial(Material material)
    {
        Material[] materials = meshRenderObject.GetComponent<SkinnedMeshRenderer>().materials;
        Material[] newMaterials = new Material[materials.Length + 1];

        materials.CopyTo(newMaterials, 0);
        newMaterials[materials.Length] = material;

        meshRenderObject.GetComponent<SkinnedMeshRenderer>().materials = newMaterials;
    }

    public void RemoveExtraMaterials()
    {
        Material[] materials = meshRenderObject.GetComponent<SkinnedMeshRenderer>().materials;
        Material[] newMaterials = new Material[1];

        newMaterials[0] = materials[0];
        meshRenderObject.GetComponent<SkinnedMeshRenderer>().materials = newMaterials;
    }

    // -----------------------
}
