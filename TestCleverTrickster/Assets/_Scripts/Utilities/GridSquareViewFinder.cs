using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSquareViewFinder
{
    /// <summary>
    /// Return the grid square object pointed by the mouse or null if the mouse doesn't point the grid
    /// </summary>
    /// <returns>The grid square object pointed by the mouse or null</returns>
    public static GameObject GetPointedGridSquare()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(mousePosition);

        // Only check for the grid elements
        LayerMask layerMask = LayerMask.GetMask("Grid");

        // we add an extra distance to be sure the ray can hit whatever is in the grid
        float rayMaxLength = Camera.main.transform.position.y + 10f;
        if (!Physics.Raycast(ray, out hit, rayMaxLength, layerMask))
            return null;

        if (hit.collider.gameObject.GetComponent<IGridSquare>() == null)
            return null;

        return hit.collider.gameObject;
    }

}
