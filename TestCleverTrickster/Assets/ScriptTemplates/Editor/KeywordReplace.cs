// /*
//
// Creation Date: 14/02/2023
// Author: Sara Jacquemin
// Project: Clean_BerserkerHomestead
//
// */

using UnityEngine;
using UnityEditor;

/// <summary>
/// Replaces tags in ScriptTemplates
/// </summary>
public class KeywordReplace : AssetModificationProcessor
{
    public static void OnWillCreateAsset(string path)
    {
        path = path.Replace(".meta", "");
        int index = path.LastIndexOf(".");
        if (index < 0) return;

        string file = path.Substring(index);
        if (file != ".cs" && file != ".js" && file != ".boo") return;

        index = Application.dataPath.LastIndexOf("Assets");
        path = Application.dataPath.Substring(0, index) + path;
        if (!System.IO.File.Exists(path)) return;

        string fileContent = System.IO.File.ReadAllText(path);

        fileContent = fileContent.Replace("#DATETIME#", System.DateTime.Today.ToString("dd/MM/yy") + "");
        fileContent = fileContent.Replace("#DEVELOPER#", System.Environment.UserName);
        fileContent = fileContent.Replace("#PROJECTNAME#", PlayerSettings.productName);

        System.IO.File.WriteAllText(path, fileContent);
        AssetDatabase.Refresh();
    }
}
